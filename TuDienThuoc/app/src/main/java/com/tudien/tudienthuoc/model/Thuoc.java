package com.tudien.tudienthuoc.model;

public class Thuoc {
    String name;

    public Thuoc() {
        super();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
