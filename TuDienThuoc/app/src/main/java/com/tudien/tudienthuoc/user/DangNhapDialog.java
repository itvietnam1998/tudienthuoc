package com.tudien.tudienthuoc.user;


import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tudien.tudienthuoc.MainActivity;
import com.tudien.tudienthuoc.R;
import com.tudien.tudienthuoc.controller.iDialog;
import com.tudien.tudienthuoc.database.DbAssetBookmark;
import com.tudien.tudienthuoc.model.Account;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DangNhapDialog extends DialogFragment implements iDialog {
    Button btnDN, btnThoat;
    EditText email, pass;
    RequestQueue requestQueue;
    Activity activity;
    StringRequest request;
    TextView quenMk;
    public static int ID_USER;
    static final String URL = "http://192.168.0.103:8080/dangnhap/";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_dialog, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        quenMk = (TextView) view.findViewById(R.id.quenMK);
        btnDN = (Button) view.findViewById(R.id.btn_dn);
        btnThoat = (Button) view.findViewById(R.id.btn_thoat);
        email = (EditText) view.findViewById(R.id.dn_email);
        pass = (EditText) view.findViewById(R.id.dn_matkhau);
        requestQueue = Volley.newRequestQueue(activity);
        btnDN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String txtEmail = email.getText().toString().trim().toLowerCase();
                String txtPass = pass.getText().toString().trim().toLowerCase();

                if (checkInput(txtEmail, txtPass) == false) {
                    Toast.makeText(activity, "Kiểm tra lại thông tin đã nhập ....", Toast.LENGTH_LONG).show();
                } else {
                    final ProgressDialog progressDialog = new ProgressDialog(activity);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage("Đang đăng nhập...");
                    progressDialog.show();
                    request = new StringRequest(Request.Method.GET, URL + txtEmail + "/" + txtPass, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getInt("account_id") != 0) {
                                    DbAssetBookmark assetBookmark = new DbAssetBookmark(activity);
                                    Account account = new Account((DangNhapDialog.ID_USER = jsonObject.getInt("account_id")), jsonObject.getString("account_email"),
                                            jsonObject.getString("account_pass"), jsonObject.getString("account_name"),
                                            jsonObject.getString("account_gender"), jsonObject.getString("account_img"));
                                    assetBookmark.insertUser(account);
                                    Toast.makeText(activity, jsonObject.getString("account_email"), Toast.LENGTH_LONG).show();
                                    MainActivity.checkLogin = true;
                                    Thread.sleep(1500);
                                    progressDialog.dismiss();
                                    dismiss();
                                } else {
                                    Toast.makeText(activity, "Email hoặc password sai !", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(activity, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                        }
                    });
                    requestQueue.add(request);
                }
            }
        });
        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        quenMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.controller.initShowDialog(new QuenMatKhauDialog());
            }
        });
        return view;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void show() {
        this.show(activity.getFragmentManager(), null);
    }

    private boolean checkInput(String email, String pass) {
        if (email.isEmpty() || pass.isEmpty()
                || isValidEmailAddress(email.trim().toLowerCase()) == false)
            return false;
        return true;
    }

    // check format email, dung form email -> true
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }
}
