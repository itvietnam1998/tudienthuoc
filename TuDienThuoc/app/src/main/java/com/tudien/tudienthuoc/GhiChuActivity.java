package com.tudien.tudienthuoc;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tudien.tudienthuoc.model.Notes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GhiChuActivity extends AppCompatActivity {

    static ArrayList<String> notes = new ArrayList<>();
    static ArrayList<Notes> notes_url = new ArrayList<>();

    static ArrayAdapter arrayAdapter;

    private String key;

    private static int id_itemToDelete_url;
    private static int id_itemToUpdate_url;

    RequestQueue requestQueue;
    JsonObjectRequest request;
    final static String URL_GET_HISTORY_NOTES = "http://192.168.1.5:8080/ghichu/";
    final static String URL_DELETE_NOTES = "http://192.168.1.5:8080/xoaghichu/";
    final static String URL_CREATE_NOTES = "http://192.168.1.5:8080/taoghichu";
    final static String URL_UPDATE_NOTES = "http://192.168.1.5:8080/capnhatghichu/";

    ListView listView;

    Button btn_tao, btn_chinhsua;
    EditText txt_Notex;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ghi_chu);
        setTitle("Ghi chú");

        btn_tao = (Button) findViewById(R.id.btn_create_notes);
        btn_chinhsua = (Button) findViewById(R.id.btn_update_notes);
        txt_Notex = (EditText) findViewById(R.id.txtNoteHere);

        key = getIntent().getIntExtra("id", 0) + "";

        requestQueue = Volley.newRequestQueue(GhiChuActivity.this);

        listView = (ListView) findViewById(R.id.listviewNotes);

        notes.clear();

        StringRequest request = new StringRequest(Request.Method.GET, URL_GET_HISTORY_NOTES + Integer.parseInt(key), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);

                    if (array != null) {
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject row = array.getJSONObject(i);

                            Notes note = new Notes();
                            note.setId_notes(row.getInt("id_notes"));
                            note.setId_account(row.getInt("id_account"));
                            note.setTime_notes(row.getString("time_notes"));
                            note.setContent_notes(row.getString("content_notes"));

                            notes_url.add(note);
                            notes.add(note.getContent_notes());
                        }
                    } else {
                        Toast.makeText(GhiChuActivity.this, "", Toast.LENGTH_LONG).show();
                    }

                    arrayAdapter = new ArrayAdapter(GhiChuActivity.this, android.R.layout.simple_list_item_1, notes);
                    listView.setAdapter(arrayAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GhiChuActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final int itemToUpdate = i;

                for (int n = 0; n < notes_url.size(); n++) {
                    if (notes.get(itemToUpdate).equalsIgnoreCase(notes_url.get(n).getContent_notes())) {
                        id_itemToUpdate_url = notes_url.get(n).getId_notes();
                    }
                }

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                final int itemToDelete = i;


                new AlertDialog.Builder(GhiChuActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Bạn sắp xóa ghi chú?")
                        .setMessage("Bạn có chắc chắn muốn xóa ghi chú này ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                for (int n = 0; n < notes_url.size(); n++) {
                                    if (notes.get(itemToDelete).equalsIgnoreCase(notes_url.get(n).getContent_notes())) {
                                        id_itemToDelete_url = notes_url.get(n).getId_notes();
                                    }
                                }

                                StringRequest request = new StringRequest(Request.Method.GET, URL_DELETE_NOTES + id_itemToDelete_url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            Toast.makeText(GhiChuActivity.this, "ghi chú xóa thành công", Toast.LENGTH_LONG).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(GhiChuActivity.this, "Kết nối mạng không ổn định", Toast.LENGTH_LONG).show();
                                    }
                                });
                                requestQueue.add(request);

                                notes.remove(itemToDelete);

                                arrayAdapter.notifyDataSetChanged();

                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return true;
            }
        });
    }


}
