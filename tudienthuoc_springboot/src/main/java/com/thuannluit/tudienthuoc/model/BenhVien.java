package com.thuannluit.tudienthuoc.model;
import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "benhvien", schema = "tudienthuoc")
public class BenhVien implements Serializable{
    private int id;
    private String location_id, name,formatted_address, latitude, longitude;
    public BenhVien(){
    }

    public BenhVien(int id, String name,String formatted_address, String latitude, String longitude){
        this.id = id;
        this.name = name;
        this.formatted_address = formatted_address;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
//    @Basic
//    @Column(name = "location_id")
//    public String getLocation_id() {
//        return location_id;
//    }
//
//    public void setLocation_id(String location_id) {
//        this.location_id = location_id;
//    }
    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Basic
    @Column(name = "formatted_address")
    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
    @Basic
    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    @Basic
    @Column(name = "longtitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
