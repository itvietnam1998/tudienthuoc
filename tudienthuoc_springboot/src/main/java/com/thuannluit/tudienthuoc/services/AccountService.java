package com.thuannluit.tudienthuoc.services;

import com.thuannluit.tudienthuoc.model.Account;
import com.thuannluit.tudienthuoc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

import java.util.List;


@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;


    // lay toan bo tai khoan
    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }

    // lay thong tin tai khoan
    public Account getInformationAccount(String email, String pass) {
        List<Account> accountList = getAllAccount();
        for (Account account : accountList) {
            if (account.getAccount_email().trim().toLowerCase().equalsIgnoreCase(email)
                    && account.getAccount_pass().trim().toLowerCase().equalsIgnoreCase(pass)) {
                return account;
            }
        }
        return null;
    }

    // Kiem tra 1 tai khoan co ton tai khong
    private boolean isAccountExist(String email, String pass) {
        List<Account> accountList = getAllAccount();
        for (Account account : accountList) {
            if (account.getAccount_email().trim().toLowerCase().equalsIgnoreCase(email)
                    && account.getAccount_pass().trim().toLowerCase().equalsIgnoreCase(pass)) {
                return true;
            }
        }
        return false;
    }

    // lay id account
    private int getID(String email) {
        int id = 0;
        List<Account> list = getAllAccount();
        for (Account account : list) {
            if (account.getAccount_email().trim().toLowerCase().equalsIgnoreCase(email))
                id = account.getAccount_id();
        }
        return id;
    }

    // Lay thong tin 1 tai khoan qua id
    // tra ve danh sach 1 phan tu
    public List<Account> findAccountById(int id) throws AccountNotFoundException {
        Account account = accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
        return Collections.singletonList(account);
    }

    // tai khoan ton tai thi lay ra tai khoan do
    public Account login(String email, String pass) {
        Account account;
        if (isAccountExist(email, pass)) {
            System.out.println("Dang nhap thanh cong");
            account = getInformationAccount(email, pass);
        } else {
            System.out.println("Email hoac password sai");
            account = new Account(0, "", "", "", "", "");
        }
        return account;
    }

    // kiem tra email co trung khong
    public String isEmailExist(String email) {
        List<Account> list = getAllAccount();
        for (Account account : list) {
            if (account.getAccount_email().trim().toLowerCase().equalsIgnoreCase(email)) return "true";
        }
        return "false";
    }


    public Account updatePassword(Account accountDetails) throws AccountNotFoundException {
        Account account2=null;
        try {
            account2 = accountRepository.findById(getID(accountDetails.getAccount_email())).orElseThrow(() -> new AccountNotFoundException(getID(accountDetails.getAccount_email())));
            account2.setAccount_pass(accountDetails.getAccount_pass());

            System.out.println("Thay doi mat khau thanh cong");

            return accountRepository.save(account2);
        } catch (Exception e) {

        }
        return account2;
    }


    // Tao moi 1 tai khoang
    public Account createAccount(Account account) {
        System.out.println("Dang ky thanh cong");
        return accountRepository.save(account);
    }

    // Cap nhat thong tin tai khoan
    public Account updateAccount(Integer id, Account accountDetails) throws AccountNotFoundException {

        Account account2 = accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
        account2.setAccount_name(accountDetails.getAccount_name());
        account2.setAccount_gender(accountDetails.getAccount_gender());
        account2.setAccount_img(accountDetails.getAccount_img());

        System.out.println("Cap nhat thong tin tai khoan thanh cong");
        return accountRepository.save(account2);
    }


    public Account getPassAccount(String email) {
        List<Account> list = getAllAccount();
        for (Account account : list) {
            if (account.getAccount_email().trim().equalsIgnoreCase(email))
                return account;
        }
        return null;
    }
}


