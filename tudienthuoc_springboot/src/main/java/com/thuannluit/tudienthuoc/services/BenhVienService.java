package com.thuannluit.tudienthuoc.services;

import com.thuannluit.tudienthuoc.model.BenhVien;
import com.thuannluit.tudienthuoc.repository.BenhVienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BenhVienService {
    @Autowired
    BenhVienRepository benhVienRepository;
    public List<BenhVien> getAllBenhVien(){
        return benhVienRepository.findAll();
    }
    public BenhVien getLocationBenhVien(int id) {
        List<BenhVien> benhVienList = getAllBenhVien();
        for(BenhVien benhVien : benhVienList){
            if(benhVien.getId() == id){
                return benhVien;
            }
        }
        return null;
    }
}
