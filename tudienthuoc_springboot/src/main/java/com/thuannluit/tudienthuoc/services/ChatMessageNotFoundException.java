package com.thuannluit.tudienthuoc.services;

public class ChatMessageNotFoundException extends Exception {
    public ChatMessageNotFoundException(Integer id) {
        super(String.format("Chat Message is not found with id : '%s'", id));
    }
}
