package com.thuannluit.tudienthuoc.controller;

import com.thuannluit.tudienthuoc.model.ChatMessage;
import com.thuannluit.tudienthuoc.services.ChatMessageService;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChatMessageController {

    private final ChatMessageService chatMessageService;

    public ChatMessageController(ChatMessageService chatMessageService) {
        this.chatMessageService = chatMessageService;
    }

    // tao tin nhan
    // dau vao 1 chuoi json
    @PostMapping(value = "/chat")
    public ChatMessage create_message(@RequestBody ChatMessage chatMessage) {
        return chatMessageService.create_message(chatMessage);
    }

    // gui tin nhan di
    // dau vao gom, id phong chat,id nguoi gui
    @GetMapping(value = "/guitinnhan/{roomchat_id}/{user_id}")
    public ChatMessage get_message(@PathVariable(value = "roomchat_id") int roomchat_id, @PathVariable(value = "user_id") int user_id) {
        return chatMessageService.get_message(roomchat_id, user_id);
    }

    // lay lich su doan hoi thoai cua phong chat
    // dau vao id phong, id user 1, id user 2
    @GetMapping(value = "/lichsuchat/{room_id}/{user_id_1}/{user_id_2}")
    public List<ChatMessage> find_chat_history_by_room_id(@PathVariable(value = "room_id") int roomchat_id
            , @PathVariable(value = "user_id_1") int user_id_1
            , @PathVariable(value = "user_id_2") int user_id_2
    ) {
        return chatMessageService.find_chat_history_by_room_id(roomchat_id, user_id_1, user_id_2);
    }

    // lay toan bo lich su chat trong database
    @GetMapping(value = "/lichsuchat")
    public List<ChatMessage> getAllHistoryMessage() {
        return chatMessageService.getAllHistoryMessage();
    }

}
