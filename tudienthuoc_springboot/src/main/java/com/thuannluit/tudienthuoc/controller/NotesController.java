package com.thuannluit.tudienthuoc.controller;

import com.thuannluit.tudienthuoc.model.Notes;
import com.thuannluit.tudienthuoc.services.NotesNotFoundException;
import com.thuannluit.tudienthuoc.services.NotesService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NotesController {

    private final NotesService notesService;

    public NotesController(NotesService notesService) {
        this.notesService = notesService;
    }

    // tao notes
        @PostMapping(value = "/taoghichu")
    public Notes createNotes(@RequestBody Notes notes) {
        return notesService.createNotes(notes);
    }

    // sua notes
    @PostMapping(value = "/capnhatghichu/{id}")
    public Notes updateNotes(@PathVariable(value = "id") Integer id, @RequestBody Notes notes) throws NotesNotFoundException {
        return notesService.updateNotes(id, notes);
    }

    // xoa notes
    @GetMapping(value = "/xoaghichu/{id}")
    public void deleteNotes(@PathVariable(value = "id") Integer id) {
        notesService.deleteNotes(id);
    }

    // lay toan bo ghi chu
    @GetMapping(value = "/ghichu")
    public List<Notes> getAllNotes() {
        return notesService.getAllNotes();
    }

    // lay ghi chu theo id tai khoan
    @GetMapping(value = "/ghichu/{id}")
    public List<Notes> getAllNotesByAccountId(@PathVariable(value = "id") Integer id) {
        return notesService.getAllNotesByIdAccount(id);
    }

}
