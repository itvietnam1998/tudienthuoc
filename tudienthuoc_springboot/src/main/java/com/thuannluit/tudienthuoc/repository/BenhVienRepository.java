package com.thuannluit.tudienthuoc.repository;

import com.thuannluit.tudienthuoc.model.BenhVien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BenhVienRepository extends JpaRepository<BenhVien, Integer> {
}
